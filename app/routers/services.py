from fastapi import APIRouter, status
from app.utils.db_operations.read import get_service_instance, get_all_service_instances

router = APIRouter()


@router.get('', status_code=status.HTTP_200_OK)
async def route_get_handle_services():
    return get_all_service_instances()


@router.get('/{ip}/{port}', status_code=status.HTTP_200_OK)
async def route_get_single_handle_service(ip: str, port: str):
    return get_service_instance(ip, port)
