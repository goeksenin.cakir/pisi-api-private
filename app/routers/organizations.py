from fastapi import APIRouter, status
from app.utils.db_operations.read import get_detailed_organizations_view, get_simple_organizations_view, \
    get_organization_by_id, get_single_organization


router = APIRouter()


@router.get('/detailed', status_code=status.HTTP_200_OK)
async def route_get_simple_organizations(with_id: bool = False):
    return get_detailed_organizations_view(with_id=with_id)


@router.get('/overview', status_code=status.HTTP_200_OK)
async def route_get_simple_organizations(with_id: bool = False):
    return get_simple_organizations_view(with_id=with_id)


@router.get('/obj_id/{obj_id}', status_code=status.HTTP_200_OK)
async def route_get_organization_by_id(obj_id: str):
    return get_organization_by_id(obj_id)


@router.get('/abbr/{org}', status_code=status.HTTP_200_OK)
async def route_get_single_organization(org: str):
    return get_single_organization(org)
