from fastapi import APIRouter, status
from app.utils.db_operations.read import search_collections

router = APIRouter()


@router.get('/{term}', status_code=status.HTTP_200_OK)
async def route_search_collections(term: str):
    return search_collections(term)
