from fastapi import APIRouter, status, BackgroundTasks
from app.utils.db_operations.read import get_prefix_only_list, get_prefixes_detailed, get_single_prefix
from app.utils.db_operations.update import trigger_update

router = APIRouter()


@router.get('', status_code=status.HTTP_200_OK)
async def route_get_prefixes_detailed():
    return get_prefixes_detailed()


# Include this function for the prefix range 21.11101-99
# Use background tasks
# @router.get('/update', status_code=status.HTTP_200_OK)
# async def route_update_db(background_tasks: BackgroundTasks):
#     op = MongoOperations()
#     return op.update_collection()

# This function is going to be used for the demo.
@router.get('/update', status_code=status.HTTP_200_OK)
async def route_add_prefix_users(background_tasks: BackgroundTasks):
    background_tasks.add_task(trigger_update)
    return {"message": "Database update is initiated."}


@router.get('/list', status_code=status.HTTP_200_OK)
async def route_get_prefix_ony_list():
    return get_prefix_only_list()


@router.get('/{handle:path}', status_code=status.HTTP_200_OK)
async def route_get_single_prefix(handle: str):
    return get_single_prefix(handle)
