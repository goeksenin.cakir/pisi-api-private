from fastapi import APIRouter, status
from app.utils.server_status import *
from app.utils.models import StatusResponse

router = APIRouter()


@router.get('/server_status', response_model=StatusResponse, response_model_exclude_unset=True,
            status_code=status.HTTP_200_OK)
async def route_get_epic_health_status(prefix: str):
    return get_epic_health_status(prefix)


@router.get('/port_status', response_model=StatusResponse, response_model_exclude_unset=True,
            status_code=status.HTTP_200_OK)
async def route_get_port_status(ip: str, port: str):
    return get_port_status(ip, port)

