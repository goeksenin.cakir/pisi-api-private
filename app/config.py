from bson.json_util import loads as bson_loads
from os import getcwd
from os.path import join
from pathlib import Path
from pydantic import BaseSettings
from pymongo import MongoClient, errors


class Settings(BaseSettings):
    timeout: int = 24 * 60 * 60  # 24 hours in second
    # mongodb_host = f'mongodb://mongo-temp:27017'
    mongodb_host = f'mongodb://127.0.0.1:27017/'
    update_disabled = False

    class Config:
        env_prefix = 'pisi_'


settings = Settings()
mongodb_client = MongoClient(settings.mongodb_host)

pid_stats_db = mongodb_client['pid-statistics']

prefixes_col = pid_stats_db['prefixes']
ip_addr_col = pid_stats_db['ip_addr_details']
orgs_col = pid_stats_db['organizations']


def load_collections():
    """"
    This function removes all the entries in all the collections so that the MongoDB instance can be populated
    by the data in the JSON files specified in this function.
    """
    try:
        prefixes_col.delete_many({})
    except:
        print('Problem deleting files')
    try:
        ip_addr_col.delete_many({})
    except:
        print('Problem deleting files')
    try:
        orgs_col.delete_many({})
    except:
        print('Problem deleting files')
    org_data = None
    prefixes_data = None
    ip_addr_data = None
    current_dir = getcwd()
    parent_dir = Path(current_dir).parent
    organizations_json = join(parent_dir, 'organizations.json')
    ip_addr_details_json = join(parent_dir, 'ip_addr_details.json')
    prefixes_json = join(parent_dir, 'prefixes.json')
    try:
        with open(organizations_json, 'r', encoding='utf-8') as f:
            org_data = f.read()
            # Serialize BSON output via bson.json_util.loads()
            org_data = bson_loads(org_data)
        with open(ip_addr_details_json, 'r', encoding='utf-8') as f:
            ip_addr_data = f.read()
            ip_addr_data = bson_loads(ip_addr_data)
        with open(prefixes_json, 'r', encoding='utf-8') as f:
            prefixes_data = f.read()
            prefixes_data = bson_loads(prefixes_data)
    # TODO: fix the too broad exception clause and handle other types of exceptions:
    except:
        print('Could not load json files')
    if org_data is not None:
        try:
            orgs_col.insert_many(org_data)
        except errors.BulkWriteError:
            print('There are duplicate documents.')
    if prefixes_data is not None:
        try:
            prefixes_col.insert_many(prefixes_data)
        except errors.BulkWriteError:
            print('There are duplicate documents.')
    if ip_addr_data is not None:
        try:
            ip_addr_col.insert_many(ip_addr_data)
        except errors.BulkWriteError:
            print('There are duplicate documents.')


