from requests import Session
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


def get_prefixes_info():
    result = []
    for i in range(1, 100):
        prefix = f'21.111{i:02}'
        sites_info = _get_handle_info(f'0.NA/{prefix}')
        if sites_info is not None:
            if len(sites_info) > 0:
                stats = {'prefix': prefix, 'sites': sites_info}
            else:
                stats = {'prefix': prefix, 'message': 'No hosting server found.'}
            result.append(stats)
    return result


def _get_handle_info(handle):
    """
    This method gets information about servers where the input handle is hosted. It follows the HS_SERV and gets all
    HS_SITEs, if they exist. If no HS_SITE is found, which means the prefix is not hosted anywhere, an empty array
    is returned. If the handle does not exist, `None` is returned.

    Parameters
    ----------
        handle : str
            The handle about which information is going to be fetched

    Returns
    ----------
        Any
            List if the information is present in in HS_SITE; None if the handle does not exist; an empty array
             if the handle is not hosted anywhere.
    """
    url = f'https://hdl.handle.net/api/handles/{handle}'
    session = Session()
    # Make three more retries if a connection failure occurs. `backoff_factor` specifies the delay between the retries:
    retry = Retry(connect=3, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    r = session.get(url)
    json = r.json()
    hs_site_values = []
    # Only process successful requests (some prefixes might not exist)
    if json['responseCode'] == 1:
        # Find HS_SERV. We assume that there will be only maximum 1 HS_SERV
        hs_serv = None
        for item in json['values']:
            if item['type'] == 'HS_SERV':
                hs_serv = item
                break
        # If HS_SERV exists, the information is stored in another handle
        if hs_serv:
            other_handle = hs_serv['data']['value']
            return _get_handle_info(other_handle)
        else:
            for item in json['values']:
                # Get information in HS_SITEs
                if item['type'] == 'HS_SITE':
                    hs_site_values.append(item)
            return hs_site_values
    return None


if __name__ == '__main__':
    get_prefixes_info()
