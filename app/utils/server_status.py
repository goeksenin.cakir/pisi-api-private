from requests import get
from socket import socket


def get_epic_health_status(prefix):
    """
    This function fetches the health status of a given prefix.

    Parameters
    ----------
        prefix : str
            Prefix whose health status is to be fetched.

    Returns
    ----------
        dict
            The health status and the relevant message are returned.
    """
    url = f'http://hdl.handle.net/api/handles/{prefix}/EPIC_HEALTHCHECK'
    r = get(url=url)
    data = r.json()
    if data['responseCode'] == 1:
        return {'status': 'OK', 'message': 'Server alive'}
    else:
        return {'status': 'No response', 'message': 'No response'}


def get_port_status(ip, port):
    """
    This function checks whether a given port on a given IP address is open.

    Parameters
    ----------
        ip : str
            IP address to connect to.
        port : int
            Port number to connect to.

    Returns
    ----------
        dict
            The port status and the relevant message message are returned.
    """
    port = int(port)
    s = socket()
    s.settimeout(5)
    result = s.connect_ex((ip, port))
    s.close()
    if result == 0:
        return {'status': 'open', 'message': 'Port Open'}
    else:
        return {'status': 'closed', 'message': 'Port Closed'}
