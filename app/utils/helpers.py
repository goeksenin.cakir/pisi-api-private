from re import search
from requests import get
from typing import Any


def fetch_ip_info(ip_address) -> Any:
    """
    This function fetches the IP address related information from the ip-api API.

    Parameters
    ----------
        ip_address : str
            IP address about which the detailed information is to be fetched

    Returns
    ----------
        data : optional
            A Python object that contains the response is returned. It can also return `None` if an exception
            occurs.
    """
    url = f'http://ip-api.com/json/{ip_address}'
    # We need to handle arbitrary connection problems that result in ConnectionError exception:
    try:
        r = get(url=url)
        # If 'content-type' doesn't exist, it will result in exception.
        if 'content-type' in r.headers.keys():
            # The content type must be JSON and the request status code must be 200 so that JSON encoded content
            # can be useful.
            if r.headers['content-type'].strip().startswith('application/json') and r.status_code == 200:
                data = r.json()
                if data['status'] == 'success':
                    return data
            return None
    except ConnectionError:
        return None


def get_first_16_bits(ip_address) -> str:
    """
    This function returns the concatenation of first 16 bits of an IP address.

    Parameters
    ----------
        ip_address : str
            IP address from which first 16 bits are to be extracted.

    Returns
    ----------
        str
            A string containing the first 16 bits of the IP address supplied as the parameter.
    """
    if type(ip_address) is not str:
        return ''
    ip_address_pattern = r'^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$'
    if search(ip_address_pattern, ip_address):
        split_string = ip_address.split('.')
        first_16_bits = split_string[0] + '.' + split_string[1]
        return first_16_bits
    return ''


def is_prefix(prefix: str) -> bool:
    """
    This function determines whether the parameter ``prefix`` is a prefix.

    Parameters
    ----------
        prefix : str
            Prefix to be validated.

    Returns
    ----------
        boolean
            True if the parameter matches the prefix pattern; False if it does not.
    """
    if type(prefix) is not str:
        return False
    prefix_pattern = r'^[0-9]{2}\.[0-9]{5}$'
    if not search(prefix_pattern, prefix):
        return False
    return True


def fetch_prefix_users() -> Any:
    """
    This function determines whether the parameter ``prefix`` is a prefix.

    Parameters
    ----------

    Returns
    ----------
        Any
            JSON data is returned if the data is successfully fetched from the URL; None if it could not.
    """
    url = f'http://lab.pidconsortium.net/pisi/dice_prefix_user.json'
    # We need to handle arbitrary connection problems that result in ConnectionError exception:
    try:
        r = get(url=url)
        # If 'content-type' doesn't exist, it will cause exception:
        if 'content-type' in r.headers.keys():
            # The content type must be JSON and the request status code must be 200 so that JSON encoded content
            # can be consumed:
            if r.headers['content-type'].strip().startswith('application/json') and r.status_code == 200:
                data = r.json()
                return data
            return None
    except ConnectionError:
        return None


def create_abbreviation(name: str, country_code: str) -> str:
    pattern = r'[A-Z]'
    abbr = ''
    counter = 0
    for i in name:
        if counter == 10:
            return abbr
        if search(pattern, i):
            abbr += i
            counter += 1
    if len(abbr) < 10:
        abbr = abbr + country_code
    return abbr


