from app.config import pid_stats_db
from app.utils.helpers import *
from app.utils.prefix_info import _get_handle_info
from app.utils.server_status import get_port_status
from bson.json_util import dumps as bson_dumps, STRICT_JSON_OPTIONS
from concurrent.futures import ThreadPoolExecutor, as_completed
from itertools import chain
from json import loads as json_loads
from json import dumps as json_dumps
from more_itertools import unique_everseen
from pymongo.errors import ConfigurationError, ConnectionFailure, OperationFailure
from time import sleep


class MongoDBUpdate:

    def __init__(self):
        self.pid_statistics = pid_stats_db
        self.prefixes_collection = self.pid_statistics['prefixes']
        self.ip_addr_collection = self.pid_statistics['ip_addr_details']
        self.orgs_collection = self.pid_statistics['organizations']
        self.users_collection = self.pid_statistics['users']
        self.servers = dict()

    def update_document(self, prefix: str, **kwargs):
        """
        This method updates a single document in the mongodb containing information about a single `prefix` passed as
        the parameter, e.g. site information and statistics. If the document with the specific `prefix` doesn't exist
        in the collection, a new document containing that specific `prefix` will be created.

        Parameters
        ----------
            prefix : str
                A prefix of type string, e.g. '21.11103'

        Returns
        ----------
            any
                None is returned if no update or insertion could be made. The result of the upsert operation is
                returned if the operation is successful.
        """
        # if not is_prefix(prefix):
        #     return False
        prefix_user = ''
        if 'prefix_user' in kwargs:
            prefix_user = kwargs['prefix_user']
        filter_query = {'prefix': prefix}
        sites_info = _get_handle_info(prefix)
        number_of_primary_sites = 0
        ip_address_set = set()
        subnets_set = set()
        # Collect all the server instances and store them into `server_instances` variable:
        server_instances = dict()
        if sites_info is None:
            return {}
        if len(sites_info) <= 0:
            return {}
        # If no information could be retrieved about prefix, ignore what is returned
        for site in sites_info:
            # Find how many primary sites
            is_primary = site['data']['value']['primarySite']
            if site['data']['value']['primarySite']:
                number_of_primary_sites += 1
            # Iterate over each server array to access to each server object
            for server in site['data']['value']['servers']:
                ip_address = server['address']
                # Collect interface and server instance information
                for interface in server['interfaces']:
                    port = str(interface['port'])
                    protocol = interface['protocol']
                    # Check if the IP address is already collected and stored in `server_instances`.
                    # If this isn't the case, then get all the interface details and map them to the IP address, and
                    # store it in `server_instances`:
                    if ip_address not in server_instances.keys():
                        temp_list = [{'protocol': protocol, 'port': port, 'isPrimary': is_primary}]
                        server_instances[ip_address] = temp_list
                    else:
                        server_instances[ip_address].append({'protocol': protocol, 'port': port,
                                                             'isPrimary': is_primary})
                ip_address_set.add(server['address'])
                subnets_set.add(get_first_16_bits(server['address']))
        # Remove duplicate server instances from list before adding them to the document:
        server_instances_result = []
        for elem in server_instances.keys():
            temp = list(unique_everseen(server_instances[elem]))
            server_instances[elem] = {'address': elem, 'interfaces': temp}
            server_instance_str = json_dumps({'address': elem, 'interfaces': temp})
            server_instances_result.append({'address': elem, 'interfaces': temp})
            if elem in self.servers.keys():
                self.servers[elem].add(server_instance_str)
            else:
                self.servers[elem] = set()
                self.servers[elem].add(server_instance_str)
        # Collect statistics information: number of sites, IP addresses and subnets
        number_of_ip_addresses = len(ip_address_set)
        number_of_sites = len(sites_info)
        number_of_subnets = len(subnets_set)
        # The last argument sets 'upsert' to True in order to insert a new document the information is
        # non-existent in the database, and if the document already exists, it will replace the existing one.
        try:
            receipt = self.prefixes_collection.update_one(filter_query, {
                '$set': {
                    'prefix': prefix, 'prefixUser': prefix_user, 'sites': sites_info,
                    'numberOfIPAddresses': number_of_ip_addresses, 'numberOfSites': number_of_sites,
                    'numberOfPrimarySites': number_of_primary_sites, 'numberOfSubnets': number_of_subnets,
                    'serverInstances': server_instances_result
                },
            }, True).raw_result
            receipt['prefix'] = prefix
            return receipt
        except (ConfigurationError, ConnectionFailure, OperationFailure):
            pass
        return {}

    def add_prefix_users(self):
        """
        This method fetches a list of prefixes and their users and then retrieves the following information:
         - the details about the prefixes from the handle API,
         - the IP addresses belonging to the prefixes, the server/service interfaces (e.g. port number and protocols)
           and the status of those interfaces.

        Returns
        ----------
            None
        """
        data = fetch_prefix_users()
        for prefix in data:
            self.update_document(prefix, prefix_user=data[prefix])
        self.update_ip_details()
        self.update_ip_interfaces()
        self.add_new_orgs()
        self.update_org_details()
        # TODO: return data instead of returning None
        return None

    def update_collections(self):
        """
        This method iterates over all the possible prefixes from 21.11101 through 21.11199 and enters the information
        about the servers into the mongodb instance one by one in each iteration by calling `update_document(prefix)`
        function. The entered information is the most actual data retrieved from handle.net API.

        Returns
        ----------
            dictionary
                A dictionary is returned that is in the same format as a document in the `prefixes` collection in the
                mongodb instance.
        """
        prefix_list = []
        for i in range(1, 100):
            prefix = f'21.111{i:02}'
            prefix_list.append(prefix)
        results = []
        processes = []
        with ThreadPoolExecutor(max_workers=10) as executor:
            for prefix in prefix_list:
                processes.append(executor.submit(self.update_document, prefix))
        for task in as_completed(processes):
            receipt = task.result()
            # Deserialize BSON output via bson.json_util.dumps()
            receipt = bson_dumps(receipt, json_options=STRICT_JSON_OPTIONS)
            # Serialize the deserialized BSON output via json.loads()
            receipt = json_loads(receipt)
            if receipt is not None:
                results.append(receipt)
        self.update_ip_details()
        self.update_ip_interfaces()
        self.add_new_orgs()
        self.update_org_details()
        return results

    def update_ip_details(self):
        """
        This method updates the details about the IP addresses in the IP addresses (`ip_addr_details`) collection.

        Returns
        ----------
            None
        """
        orgs_dict = dict()
        ip_details = dict()
        for org_doc in self.orgs_collection.find({}, {'_id': 0}):
            orgs_dict[org_doc['ip-api_name']] = org_doc
        counter = 0
        for ip in self.servers.keys():
            counter += 1
            # Wait 65 seconds in order to avoid getting banned by IP-API due to exceeding API consumption limits:
            if counter % 45 == 0:
                sleep(65)
            result = fetch_ip_info(ip)
            ip_details[ip] = result
            if result is None:
                continue
            # Use the organization name (specified in the value of 'org' field name) as the foreign key:
            if result['org'] in orgs_dict.keys():
                result['org_abbr'] = orgs_dict[result['org']]['abbr']
            # If the organization name doesn't exist or an empty string in the output of IP-API, use ISP name
            # (specified in the value of 'isp' field name) as foreign key:
            else:
                if result['isp'] in orgs_dict.keys():
                    result['org_abbr'] = orgs_dict[result['isp']]['abbr']
                # If the foreign key (which is either organization name or ISP name) doesn't match any document
                # in the organizations collections, create a new abbreviation based on first 10 letters and use
                # the organization name or ISP name (as fetched from IP-API) as the view name.
                else:
                    if result['org'] != '':
                        result['org_abbr'] = create_abbreviation(result['org'], result['countryCode'])
                    else:
                        result['org_abbr'] = create_abbreviation(result['isp'], result['countryCode'])
            result['prefixes'] = []
            for doc in self.prefixes_collection.find({'serverInstances.address': ip}, {'_id': 0, 'sites': 0}):
                result['prefixes'].append(doc)
            # The last argument sets 'upsert' to True in order to insert a new document the information is non-existent
            # in the database, and if the document already exists, it will replace the existing one with the new
            # information fetched from IP-API.
            # TODO: return data instead of returning `None`
            try:
                self.ip_addr_collection.update_one(
                    {'address': ip}, {'$set': {
                        'address': result['query'],
                        'as': result['as'],
                        'city': result['city'],
                        'country': result['country'],
                        'countryCode': result['countryCode'],
                        'isp': result['isp'],
                        'lat': result['lat'],
                        'lon': result['lon'],
                        'org': result['org'],
                        'region': result['region'],
                        'regionName': result['regionName'],
                        'timezone': result['timezone'],
                        'zip': result['zip'],
                        'org_abbr': result['org_abbr'],
                        'prefixes': result['prefixes']
                    }}, True
                )
            except (ConfigurationError, ConnectionFailure, OperationFailure):
                pass

    def update_ip_interfaces(self):
        """
        This method updates the server interfaces of the IP addresses in the IP addresses collection.

        Returns
        ----------
            list
                A list of all prefixes with all the relevant details
        """
        # Iterate over all the IP addresses in `self.servers`, extract all the server instances, remove the repeated
        # elements, get port status for each port in the server (i.e. IP address), and finally update the collection:
        for ip_addr in self.servers.keys():
            temp_list = self.servers[ip_addr]
            server_instances_list = []
            for elem in temp_list:
                temp_json = json_loads(elem)
                server_instances_list.append(temp_json['interfaces'])
            # Remove duplicates from server instances list
            server_instances_list = list(unique_everseen(list(chain(*server_instances_list))))
            for item in server_instances_list:
                status_result = get_port_status(ip_addr, item['port'])
                item['status'] = status_result['status']
            try:
                self.ip_addr_collection.update_one(
                    {'address': ip_addr}, {'$set': {'interfaces': server_instances_list}}, True)
            except (ConfigurationError, ConnectionFailure, OperationFailure):
                pass

        # Update the interfaces of each server instance (`serverInstances` and `sites.data.value.servers` fields)
        # in `prefixes` collection:
        for ip_addr_doc in self.ip_addr_collection.find({}, {'_id': 0}):
            ip_addr = ip_addr_doc['address']
            ip_addr_doc_interfaces = ip_addr_doc['interfaces']
            for prefix_doc in self.prefixes_collection.find({'serverInstances.address': ip_addr}):
                prefix_doc_id = prefix_doc['_id']
                server_instances_list = []
                for server_instance in prefix_doc['serverInstances']:
                    if ip_addr == server_instance['address']:
                        server_instance['interfaces'] = ip_addr_doc_interfaces
                    server_instances_list.append(server_instance)
                sites = []
                for j in range(len(prefix_doc['sites'])):
                    site = prefix_doc['sites'][j]
                    data_value_servers = []
                    for i in range(len(site['data']['value']['servers'])):
                        server = site['data']['value']['servers'][i]
                        if ip_addr == server['address']:
                            for h in range(len(server['interfaces'])):
                                str_port = str(server['interfaces'][h]['port'])
                                updating_intf = list(filter(lambda intf:
                                                            (intf['port'] == str_port), ip_addr_doc_interfaces))
                                updating_intf = list(filter(lambda intf:
                                                            (intf['protocol'] == server['interfaces'][h]['protocol']),
                                                            updating_intf))
                                if len(updating_intf) > 0:
                                    server['interfaces'][h]['status'] = updating_intf[0]['status']
                                    server['interfaces'][h]['isPrimary'] = updating_intf[0]['isPrimary']
                        data_value_servers.append(server)
                    site['data']['value']['servers'] = data_value_servers
                    sites.append(site)
                try:
                    self.prefixes_collection.update_one(
                        {'_id': prefix_doc_id},
                        {'$set':
                            {
                                'sites': sites,
                                'serverInstances': server_instances_list,
                            }
                        }
                    )
                except (ConfigurationError, ConnectionFailure, OperationFailure):
                    pass
        # TODO: return data instead of returning None
        return None

    def add_new_orgs(self):
        """
        This method adds new organizations to the `organizations` collection from the IP addresses collection
        (`ip_addr_details`), which is populated with the data fetched from IP-API.

        Returns
        ----------
            None
        """
        added_orgs = []
        for ip_addr_doc in self.ip_addr_collection.find({}, {'_id': 0}):
            # Use the organization name (specified in the value of 'org' field name) as the foreign key.
            # If the organization name doesn't exist or an empty string in the output of IP-API, use the ISP name
            # (specified in the value of 'isp' field name) as foreign key:
            org_name = ip_addr_doc['org']
            org_isp = ip_addr_doc['isp']
            name_num_of_matches = self.orgs_collection.count_documents({'ip-api_name': org_name})
            isp_num_of_matches = self.orgs_collection.count_documents({'ip-api_name': org_isp})
            # If the organization is already in the `organizations` collection, then skip this document:
            if name_num_of_matches > 0 or isp_num_of_matches > 0:
                continue
            if org_name in added_orgs or org_isp in added_orgs:
                continue
            # If the organization name is empty, then use the name of the ISP as the foreign key in organizations
            # collection:
            if org_name == '':
                added_orgs.append(org_isp)
                ip_api_name = org_isp
            else:
                added_orgs.append(org_name)
                ip_api_name = org_name
            city = ip_addr_doc['city']
            country = ip_addr_doc['country']
            # TODO: return data instead of returning None
            try:
                self.orgs_collection.update_one(
                    {'ip-api_name': ip_api_name},
                    {
                        '$set':
                            {
                                'name': ip_api_name,
                                'abbr': '',
                                'ip-api_name': ip_api_name,
                                'country': country,
                                'city': city,
                                'is_root': True,
                            }
                    },
                    True)
            except (ConfigurationError, ConnectionFailure, OperationFailure):
                pass
        return None

    def update_org_details(self):
        """
        This method updates the organization details by adding the prefixes and the server instances.

        Returns
        ----------
            None
        """
        for org_doc in self.orgs_collection.find({}, {'_id': 0}):
            service_instances = dict()
            prefixes = []
            for ip_addr_doc in self.ip_addr_collection.find({'org': org_doc['ip-api_name']}, {'_id': 0}):
                ip_addr = ip_addr_doc['address']
                interfaces = ip_addr_doc['interfaces']
                if ip_addr in service_instances.keys():
                    service_instances[ip_addr].append(interfaces)
                else:
                    service_instances[ip_addr] = interfaces
                temp_set = ip_addr_doc['prefixes']
                prefixes += temp_set
            prefixes = list(unique_everseen(prefixes))
            # TODO: return data instead of returning None
            try:
                self.orgs_collection.update_one(
                    {'ip-api_name': org_doc['ip-api_name']},
                    {
                        '$set':
                            {
                                'server_instances': service_instances,
                                'prefixes': prefixes
                            }
                    },
                    True)
            except (ConfigurationError, ConnectionFailure, OperationFailure):
                pass
            for ip_addr_doc in self.ip_addr_collection.find({'isp': org_doc['ip-api_name']}, {'_id': 0}):
                ip_addr = ip_addr_doc['address']
                interfaces = ip_addr_doc['interfaces']
                if ip_addr in service_instances.keys():
                    service_instances[ip_addr].append(interfaces)
                else:
                    service_instances[ip_addr] = interfaces
                temp_set = ip_addr_doc['prefixes']
                prefixes += temp_set
            prefixes = list(unique_everseen(prefixes))
            # TODO: return data instead of returning None
            try:
                self.orgs_collection.update_one(
                    {'ip-api_name': org_doc['ip-api_name']},
                    {
                        '$set':
                            {
                                'server_instances': service_instances,
                                'prefixes': prefixes
                            }
                    },
                    True)
            except (ConfigurationError, ConnectionFailure, OperationFailure):
                pass
        return None


def trigger_update():
    op = MongoDBUpdate()
    op.add_prefix_users()
    # TODO: return data instead of returning None
    return None
