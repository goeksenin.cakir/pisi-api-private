from app.config import pid_stats_db
# from app.utils.helpers import *
from bson import objectid, ObjectId
from pymongo.errors import ConfigurationError, ConnectionFailure, OperationFailure
from re import escape


def get_ip_details(ip_address: str):
    """
    This function updates the server interfaces of the IP addresses in the IP addresses collection.

    Parameters
    ----------
        ip_address : str
            IP Address to be searched

    Returns
    ----------
        list
            A list of all prefixes with all the relevant details
    """
    ip_addr_collection = pid_stats_db['ip_addr_details']
    for document in ip_addr_collection.find({'address': ip_address}, {'_id': 0}).sort('address', 1):
        return document
    return None


def get_prefixes_detailed() -> list:
    """
    This function retrieves detailed information about all prefixes from the MongoDB.

    Returns
    ----------
        list
            A list of all prefixes with all the relevant details
    """
    prefixes_collection = pid_stats_db['prefixes']
    result = []
    for document in prefixes_collection.find({}, {'_id': 0}).sort('prefix', 1):
        result.append(document)
    return result


def get_prefix_only_list() -> list:
    """
    This function retrieves general information about all prefixes from the MongoDB.

    Returns
    ----------
        dict or None
    """
    prefixes_collection = pid_stats_db['prefixes']
    result = []
    try:
        for document in prefixes_collection.find({}, {'_id': 0, 'prefix': 1, 'numberOfIPAddresses': 1,
                                                      'numberOfSites': 1, 'numberOfPrimarySites': 1,
                                                      'numberOfSubnets': 1}).sort('prefix', 1):
            result.append(document)
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass
    return result


def get_simple_organizations_view(**kwargs):
    """
    This function retrieves general information about all organizations from the MongoDB.

    Returns
    ----------
        list
            A list of all organizations
    """
    orgs_collection = pid_stats_db['organizations']
    results = []
    with_id = False
    if 'with_id' in kwargs:
        with_id = kwargs['with_id']
    if with_id:
        try:
            for doc in orgs_collection.find({}, {'prefixes': 0, 'server_instances': 0}).sort('name', 1):
                if 'is_root' not in doc.keys():
                    continue
                if not doc['is_root']:
                    continue
                doc['_id'] = str(doc['_id'])
                results.append(doc)
            return results
        except (ConfigurationError, ConnectionFailure, OperationFailure):
            pass
    else:
        try:
            for doc in orgs_collection.find({}, {'_id': 0, 'prefixes': 0, 'server_instances': 0}).sort('name', 1):
                results.append(doc)
            return results
        except (ConfigurationError, ConnectionFailure, OperationFailure):
            pass


def get_organization_by_id(org_id: str):
    orgs_collection = pid_stats_db['organizations']
    if objectid.ObjectId.is_valid(org_id):
        org_id = ObjectId(org_id)
    else:
        return None
    try:
        org_info = orgs_collection.find_one({'_id': org_id})
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass
    if org_info is not None:
        org_info['_id'] = str(org_info['_id'])
    return org_info


def get_detailed_organizations_view(**kwargs):
    """
    This function retrieves information all service instances from the MongoDB.

    Returns
    ----------
        list
            A list of all service instances is returned.
    """
    orgs_collection = pid_stats_db['organizations']
    results = []
    with_id = False
    if 'with_id' in kwargs:
        with_id = kwargs['with_id']
    if with_id:
        try:
            for doc in orgs_collection.find({}).sort('name', 1):
                doc['_id'] = str(doc['_id'])
                results.append(doc)
            return results
        except (ConfigurationError, ConnectionFailure, OperationFailure):
            pass
    else:
        try:
            for doc in orgs_collection.find({}, {'_id': 0}).sort('name', 1):
                results.append(doc)
            return results
        except (ConfigurationError, ConnectionFailure, OperationFailure):
            pass


def get_all_service_instances():
    """
    This function retrieves detailed information about all organizations from the MongoDB.

    Returns
    ----------
        list
            A list of all organizations is returned.
    """
    ip_addr_collection = pid_stats_db['ip_addr_details']
    results = []
    try:
        for doc in ip_addr_collection.find({'interfaces.protocol': 'TCP'},
                                           {'_id': 0, 'address': 1, 'org': 1,
                                            'interfaces': 1}).sort('address', 1):
            results.append(doc)
        return results
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass


def get_service_instance(ip_addr: str, port: str):
    """
    This function retrieves a single server instance based on the combination of ``ip_addr`` and ``port`` from
    the MongoDB.

    Returns
    ----------
        dict
            A dictionary is returned that contains information about the IP address and the prefix in the service
            instance that will later be consumed by the frontend service.
    """
    ip_addr_collection = pid_stats_db['ip_addr_details']
    orgs_collection = pid_stats_db['organizations']
    try:
        ip_addr_info = ip_addr_collection.find_one({'address': ip_addr, 'interfaces.port': port}, {'_id': 0})
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass

    # Foreign key is either `org` field or `isp` field:
    if ip_addr_info['org'] == '':
        foreign_key = ip_addr_info['isp']
        try:
            org_info = orgs_collection.find_one(
                {'ip-api_name': foreign_key}, {'_id': 0}
            )
        except (ConfigurationError, ConnectionFailure, OperationFailure):
            pass
    else:
        foreign_key = ip_addr_info['org']
        # Query the `organizations` collection to retrieve the relevant details:
        try:
            org_info = orgs_collection.find_one(
                {'ip-api_name': foreign_key}, {'_id': 0}
            )
        except (ConfigurationError, ConnectionFailure, OperationFailure):
            pass
        if org_info is None:
            foreign_key = ip_addr_info['isp']
            try:
                org_info = orgs_collection.find_one(
                    {'ip-api_name': foreign_key}, {'_id': 0}
                )
            except (ConfigurationError, ConnectionFailure, OperationFailure):
                pass
    # Add port information to the data that will be returned:
    if ip_addr_info is not None:
        ip_addr_info['port'] = port
    prefix_info = ip_addr_info['prefixes']
    del ip_addr_info['prefixes']
    result = {'ip_details': ip_addr_info, 'prefixes': prefix_info, 'org_details': org_info}
    return result


def get_single_organization(org_abbr: str):
    """
    This function retrieves information about a single `organization` whose abbreviation passed as the parameter
    and returns this information in the JSON format that will later be consumed by the frontend service.

    Parameters
    ----------
        org_abbr : str
            An abbreviation for an organization, e.g. 'GWDG'

    Returns
    ----------
        dict
            A dictionary is returned that is in the same format as a JSON file later be consumed
            by the frontend service.
    """
    orgs_collection = pid_stats_db['organizations']
    prefixes_collection = pid_stats_db['prefixes']
    ip_addr_collection = pid_stats_db['ip_addr_details']
    org_name = org_abbr.upper()
    org_info = orgs_collection.find_one({'abbr': org_name}, {'_id': 0})
    if org_info is None:
        return []
    # Search and get prefix information for the organization in the `prefixes` collection:
    try:
        prefix_info = prefixes_collection.find({},
                                               {'_id': 0, 'prefix': 1, 'serverInstances': 1, 'prefixUser': 1,
                                                'numberOfIPAddresses': 1, 'numberOfSites': 1,
                                                'numberOfPrimarySites': 1, 'numberOfSubnets': 1})
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass
    result = []
    for prefix_document in prefix_info:
        prefix = prefix_document['prefix']
        prefix_user = prefix_document['prefixUser']
        if 'serverInstances' not in prefix_document.keys():
            continue
        server_instances = prefix_document['serverInstances']
        for server in server_instances:
            for elem_interface in server['interfaces']:
                if elem_interface['protocol'] != 'TCP':
                    continue
                protocol = elem_interface['protocol']
                port = elem_interface['port']
                ip_addr = server['address']
                num_of_matches = 0
                try:
                    num_of_matches = ip_addr_collection.count_documents(
                        {'address': ip_addr, 'org': org_info['ip-api_name']}
                    )
                except (ConfigurationError, ConnectionFailure, OperationFailure):
                    pass
                if num_of_matches <= 0:
                    continue
                result.append({
                    'prefix': prefix, 'prefixUser': prefix_user, 'ipAddress': ip_addr, 'protocol': protocol,
                    'port': port, 'numberOfIPAddresses': prefix_document['numberOfIPAddresses'],
                    'numberOfSites': prefix_document['numberOfSites'],
                    'numberOfPrimarySites': prefix_document['numberOfPrimarySites'],
                    'numberOfSubnets': prefix_document['numberOfSubnets'],
                })
    return {'name': org_info['name'], 'abbr': org_abbr, 'country': org_info['country'],
            'city': org_info['city'], 'prefixes': result}


def get_single_prefix(prefix: str):
    """
    This function retrieves information about a single `prefix` passed as the parameter and returns this information
    in the JSON format that will later be consumed by the frontend service.

    Parameters
    ----------
        prefix : str
            A prefix of type string, e.g. '21.11103'

    Returns
    ----------
        dict
            A dictionary is returned that is in the same format as a JSON file later be consumed by
            the frontend service.
    """
    prefixes_collection = pid_stats_db['prefixes']
    ip_addresses = []
    try:
        sites_info = prefixes_collection.find_one({'prefix': prefix})
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass
    description = ''

    if sites_info is None:
        return {}
    if 'sites' not in sites_info.keys():
        return {}
    if len(sites_info) <= 0:
        return {}

    if 'prefixUser' in sites_info.keys():
        prefix_user = sites_info['prefixUser']
    else:
        prefix_user = ''

    for site in sites_info['sites']:
        if site['data']['value']['attributes']:
            for attr in site['data']['value']['attributes']:
                if attr['name'] == 'desc':
                    description = attr['value']

        for server in site['data']['value']['servers']:
            ip_address = server['address']
            ip_address_details = get_ip_details(ip_address)
            is_primary = site['data']['value']['primarySite']
            server_interfaces = server['interfaces']
            ip_addresses.append({'ipAddress': ip_address, 'isPrimary': is_primary,
                                 'interfaces': server_interfaces, 'desc': description,
                                 'details': ip_address_details})
    return {
        'prefix': prefix, 'prefixUser': prefix_user, 'ipAddresses': ip_addresses,
        'numberOfSubnets': sites_info['numberOfSubnets'],
        'numberOfIPAddresses': sites_info['numberOfIPAddresses'],
        'numberOfPrimarySites': sites_info['numberOfPrimarySites'],
        'numberOfSites': sites_info['numberOfSites']
    }


def search_collections(search_term: str):
    """
    This function searches the prefixes and the organizations collections with a search term supplied as parameter.

    Parameters
    ----------
        search_term : str
            Term to be searched in all collections, e.g. '21.11101', 'Max Planck' or 'GWDG'

    Returns
    ----------
        list
            A list of matched item
    """
    prefixes_collection = pid_stats_db['prefixes']
    orgs_collection = pid_stats_db['organizations']
    ip_addr_collection = pid_stats_db['ip_addr_details']
    # Escape the search term provided by the user in order to prevent certain character or character groups in
    # the search term from being interpreted as special regex terms:
    search_term = escape(search_term)
    results = []
    try:
        for doc in orgs_collection.find(
                {'$or': [
                    # $options: 'i' is for case insensitive search
                    {'name': {'$regex': search_term, '$options': 'i'}},
                    {'abbr': {'$regex': search_term, '$options': 'i'}}
                ]},
                {'_id': 0}).sort('name', 1):
            results.append({'org': doc})
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass

    try:
        for doc in prefixes_collection.find(
                {'prefix': {'$regex': search_term, '$options': 'i'}},
                {'_id': 0}).sort('prefix', 1):
            results.append({'prefix': doc})
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass

    # We need a strategy to differentiate the results found in the port from the results found in
    # the IP address. One could be that there are two separate queries: one for the IP address field
    # and one for the port number field:

    try:
        for doc in ip_addr_collection.aggregate(
                [
                    {'$match': {'interfaces': {'$elemMatch': {'port': {'$regex': search_term, '$options': 'i'}}}}},
                    {'$unwind': '$interfaces'},
                    {'$match': {'interfaces.port': {'$regex': search_term, '$options': 'i'}}},
                    {'$project': {'_id': 0}}
                ]):
            results.append({'port': doc})
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass

    try:
        for doc in ip_addr_collection.find({'address': {'$regex': search_term, '$options': 'i'}}, {'_id': 0}):
            results.append({'ip_addr': doc})
    except (ConfigurationError, ConnectionFailure, OperationFailure):
        pass

    return results
