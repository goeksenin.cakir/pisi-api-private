import pytest

from app.utils.helpers import *


@pytest.mark.parametrize(
    'test_ip_address,expected_output',
    [('134.76.30.193', '134.76'), ('145.100.31.156', '145.100'),
     (55, ''), pytest.param('', 42, marks=pytest.mark.xfail),
     pytest.param('127.0.0.1', '127.0.0', marks=pytest.mark.xfail)]
)
def test_get_first_16_bits(test_ip_address, expected_output):
    assert get_first_16_bits(test_ip_address) == expected_output