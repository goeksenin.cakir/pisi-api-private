from app.utils.db_operations.read import *
from app.utils.db_operations.update import *

import pytest


@pytest.mark.parametrize(
    'test_prefix, expected_output',
    [('', {}), ('99.99999', {})]
)
def test_get_single_prefix(test_prefix, expected_output):
    assert type(get_single_prefix(test_prefix)) == type(expected_output)


@pytest.mark.parametrize(
    'test_prefix, expected_output',
    [('21.15108', {}), ('', {}), ('99.99999', {})]
)
def test_update_document(test_prefix, expected_output):
    o = MongoDBUpdate()
    assert type(o.update_document(test_prefix)) == type(expected_output)
