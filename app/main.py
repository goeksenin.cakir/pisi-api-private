from app.routers import organizations, prefixes, search, services, status_checks
from apscheduler.schedulers.background import BackgroundScheduler
from config import load_collections
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from utils.db_operations.update import trigger_update
import uvicorn


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)

app.include_router(organizations.router, prefix='/organizations')
app.include_router(prefixes.router, prefix='/prefixes')
app.include_router(search.router, prefix='/search')
app.include_router(services.router, prefix='/services')
app.include_router(status_checks.router, prefix='/status')


@app.on_event('startup')
def init_jobs():
    load_collections()
    scheduler = BackgroundScheduler()
    scheduler.add_job(trigger_update, 'interval', hours=1)
    scheduler.start()


if __name__ == '__main__':
    # uvicorn.run(app, host='0.0.0.0', port=80)
    uvicorn.run(app, host='0.0.0.0', port=8085)
