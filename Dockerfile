FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

# Install requirements
COPY requirements.txt .
RUN apt-get --allow-releaseinfo-change update
RUN apt-get install -y ca-certificates
RUN wget https://pki.pca.dfn.de/grid-root-ca/pub/cacert/cacert.crt
RUN mv cacert.crt /usr/local/share/ca-certificates/cacert_dfn.crt
RUN update-ca-certificates
RUN curl https://pypi.org:443
RUN python -m pip install --upgrade pip
RUN python -m pip install --no-cache-dir -r requirements.txt

COPY ./app /app/app
COPY organizations.json /app/app
COPY ip_addr_details.json /app/app
COPY prefixes.json /app/app
